package pl.codementors.lambdas;


import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Arrays;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class LambdasMain {

    public static void main(String[] args){

        List<String> dbnames = Arrays.asList("SonGokuh", "SonGohan", "Vegeta", "Trunks", "Piccolo", "freeza", "majinBuu", "cell", "android18", "" );

        System.out.println("Task1:");
        task1(dbnames);
        System.out.println("\nTask2:");
        task2(dbnames);
        System.out.println("\nTask3:");
        task3(dbnames);
        System.out.println("\nTask4:");
        task4(dbnames);
        System.out.println("\nTask5:");
        task5(dbnames);
        System.out.println("\nTask6:");
        task6(dbnames);
        System.out.println("\nTask7:");
        task7(dbnames);
        System.out.println("\nTask8:");
        task8(dbnames);
        System.out.println("\nTask9:");
        task9(dbnames);
        System.out.println("\nTask10:");
        task10(dbnames);
        System.out.println("\nTask11:");
        task11(dbnames);
        System.out.println("\nTask12:");
        task12(dbnames);
        System.out.println("\nTask13:");
        task13(dbnames);
        System.out.println("\nTask14:");
        task14(dbnames);
        System.out.println("\nTask15:");
        task15(dbnames);


    }

    public static void task1(List<String> list){
        list.forEach(value -> System.out.print(value + ", "));
    }

    public static void task2(List<String> list){
        list.stream().mapToInt(String::length).forEach(value->System.out.print(value + ", "));
    }

    public static void task3(List<String> list){
        System.out.print(list.stream().mapToInt(String::length).sum());
    }

    public static void task4(List<String> list){
        System.out.print(list.stream().mapToInt(String::length).min().getAsInt());

    }

    public static void task5(List<String> list) {
        System.out.print(list.stream().mapToInt(String::length).max().getAsInt());
    }

    public static void task6(List<String> list) {
        list.stream().filter(value->value.length()>0).
                filter(value->(Character.isUpperCase(value.charAt(0)))).
                forEach(value->System.out.print(value+", "));
    }

    public static void task7(List<String> list) {
        list.stream().filter(value-> value.length() >0).
                forEach(value->System.out.print(value+", " ));
    }

    public static void task8(List<String> list) {
        list.stream().filter(value-> value.startsWith("a")).
                forEach(value->System.out.print(value+", " ));
    }

    public static void task9(List<String> list) {
        list.stream().filter(value-> value.endsWith("a")).
                forEach(value->System.out.print(value+", " ));
    }

    public static void task10(List<String> list) {
        list.stream().sorted(String::compareToIgnoreCase).
                forEach(value->System.out.print(value+", "));
    }

    public static void task11(List<String> list) {
        list.stream().sorted(Comparator.comparing(String::length)).
                forEach(value->System.out.print(value+", "));
    }

    public static void task12(List<String> list){
        list.stream().sorted(Comparator.comparing(String::length).reversed()).
                mapToInt(String::length).
                forEach(value->System.out.print(value + ", "));
    }

    public static void task13(List<String> list){
        list.stream().filter(value->value.length()>0).
                sorted(Comparator.comparing(String::length)).
                mapToInt(String::length).
                forEach(value->System.out.print(value + ", "));
    }

    public static void task14(List<String> list){
        list.stream().filter(value->value.length()>0).
                collect(Collectors.toSet()).forEach(value->System.out.print(value+", "));
    }

    public static void task15(List<String> list) {
        list.stream().sorted(String::compareToIgnoreCase).
                collect(Collectors.toList()).stream().
                forEach(value->System.out.print(value+", "));
    }
}

